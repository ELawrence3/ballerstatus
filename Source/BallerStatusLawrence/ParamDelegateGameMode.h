// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "ParamDelegateGameMode.generated.h"

/**
 * 
 */
DECLARE_MULTICAST_DELEGATE(FMulticastDelegateSignature)
DECLARE_DELEGATE_OneParam(FParamDelegateSignature, FLinearColor)
DECLARE_DELEGATE(FStandardDelegateSignature)
UCLASS()
class BALLERSTATUSLAWRENCE_API AParamDelegateGameMode : public AGameMode
{
	GENERATED_BODY()
	
public:
		FStandardDelegateSignature MyStandardDelegate;
		FParamDelegateSignature MyParameterDelegate;	
		FMulticastDelegateSignature MyMulticastDelegate;
};
