// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "BallerStatusLawrenceHUD.generated.h"

UCLASS()
class ABallerStatusLawrenceHUD : public AHUD
{
	GENERATED_BODY()

public:
	ABallerStatusLawrenceHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

