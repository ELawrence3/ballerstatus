// Fill out your copyright notice in the Description page of Project Settings.

#include "DelegateListener.h"
#include "ParamDelegateGameMode.h"
#include "Engine.h"


// Sets default values
ADelegateListener::ADelegateListener()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PointLight = CreateDefaultSubobject<UPointLightComponent>("PointLight");
	PointLight->SetBloomTint(FColor(255, 0, 0));
	RootComponent = PointLight;
	PointLight->SetVisibility(false);
}

void ADelegateListener::EnableLight()
{
	PointLight->SetVisibility(true);
}

// Called when the game starts or when spawned
void ADelegateListener::BeginPlay()
{
	Super::BeginPlay();

	UWorld* TheWorld = GetWorld();
	if (TheWorld != nullptr) 
	{
		AGameMode* GameMode = (AGameMode*)TheWorld->GetAuthGameMode();
		AParamDelegateGameMode* MyGameMode = Cast<AParamDelegateGameMode>(GameMode);
		if (MyGameMode != nullptr)
		{
			MyGameMode->MyStandardDelegate.BindUObject(this, &ADelegateListener::EnableLight);
		}

	}
	
}

void ADelegateListener::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason); UWorld* TheWorld = GetWorld();
	if (TheWorld != nullptr) 
	{
		AGameMode* GameMode = (AGameMode*)TheWorld->GetAuthGameMode();
		AParamDelegateGameMode* MyGameMode = Cast<AParamDelegateGameMode>(GameMode);
		if (MyGameMode != nullptr) 
		{ 
			MyGameMode->MyStandardDelegate.Unbind();
		}
	}
}




// Called every frame
void ADelegateListener::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

