// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "STextBlock.h"
#include "SlateBasics.h" 
#include "AttributeGameMode.generated.h"

/**
 * 
 */
UCLASS()
class BALLERSTATUSLAWRENCE_API AAttributeGameMode : public AGameMode
{
	GENERATED_BODY()
	
private:
	TSharedPtr<SVerticalBox> Widget;

public:
	virtual void BeginPlay() override;
	FText GetButtonLabel() const;
	
	
};
