// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "Engine/Font.h"
#include "SlateBasics.h" 
#include "SButton.h" 
#include "STextBlock.h"
#include "TimerManager.h"
#include "ClickEventGameMode.generated.h"

/**
 * 
 */
UCLASS()
class BALLERSTATUSLAWRENCE_API AClickEventGameMode : public AGameMode
{
	GENERATED_BODY()
	
private:
	TSharedPtr<SVerticalBox> Widget;
	TSharedPtr<STextBlock> ButtonLabel;

public:
	virtual void BeginPlay() override;
	FReply ButtonClicked();
	
	
};
