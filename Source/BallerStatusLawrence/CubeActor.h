// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h" 
#include "RuntimeMeshComponent.h"
#include "Components/BoxComponent.h"
#include "CubeActor.generated.h"

UCLASS()
class BALLERSTATUSLAWRENCE_API ACubeActor : public AActor
{
	GENERATED_BODY()
	

public:	
	// Sets default values for this actor's properties
	ACubeActor();
	virtual void GenerateBoxMesh();
	virtual void GenerateCollision(FVector BoxRadius);
	virtual void CreateBoxMesh(FVector BoxRadius, TArray <FVector> & Vertices, TArray <int32> & Triangles, TArray <FVector> & Normals, TArray <FVector2D> & UVs, TArray <FProcMeshTangent> & Tangents, TArray <FColor> & Colors);
	virtual void Tick(float DeltaTime) override;
	void PostActorCreated() override;
	void PostLoad() override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UProceduralMeshComponent* mesh;//changed from runtime to procedural
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UBoxComponent* box;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool Randomize;



protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	
	
};
