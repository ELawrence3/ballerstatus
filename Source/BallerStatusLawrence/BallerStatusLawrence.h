// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "CookbookStyle.h"
class UE4CookbookGameModule : public FDefaultGameModuleImpl
{
	virtual void StartupModule() override 
	{
		CookbookStyle::Initialize(); 
	};
	virtual void ShutdownModule() override 
	{
		CookbookStyle::Shutdown();
	}; 
};