// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine.h"
#include "GameFramework/Actor.h"
#include "ParamDelegateListener.generated.h"

UCLASS()
class BALLERSTATUSLAWRENCE_API AParamDelegateListener : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AParamDelegateListener();

	UFUNCTION()
		void BeginPlay();
	void SetLightColor(FLinearColor LightColor, bool EnableLight);
	UPROPERTY()
		UPointLightComponent* PointLight;


};