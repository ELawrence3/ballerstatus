// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BallerStatusLawrenceGameMode.generated.h"

DECLARE_DELEGATE(FStandardDelegateSignature)
UCLASS(minimalapi)
class ABallerStatusLawrenceGameMode : public AGameModeBase
{
	GENERATED_BODY()

		FStandardDelegateSignature MyStandardDelegate;
	

public:
	ABallerStatusLawrenceGameMode();
};