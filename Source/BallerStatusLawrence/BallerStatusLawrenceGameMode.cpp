// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "BallerStatusLawrenceGameMode.h"
#include "BallerStatusLawrenceHUD.h"
#include "BallerStatusLawrenceCharacter.h"
#include "UObject/ConstructorHelpers.h"

ABallerStatusLawrenceGameMode::ABallerStatusLawrenceGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ABallerStatusLawrenceHUD::StaticClass();
}
